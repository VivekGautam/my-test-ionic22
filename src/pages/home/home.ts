import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public loginForm;
  submitAttempt: boolean = false;
  
  constructor(public navCtrl: NavController, public frmbuilder: FormBuilder) {
    this.loginForm = frmbuilder.group({
      username: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(3), Validators.required])]
    })
    console.log("data ")

  } 
  loginUser  () {
    this.submitAttempt = true;

    if (!this.loginForm.valid) {
      console.log(this.loginForm.value);

    } 
    else {
      console.log(this.loginForm.value);
      

    }
  }

}
